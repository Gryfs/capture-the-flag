﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject BStart;
    public GameObject QuitB;
    public GameObject ControlB;
    public GameObject GText;
    public GameObject BBack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //démarrer le jeu (scene suivante)
    }
    public void Quit()
    {
        Application.Quit(); //quitter l'application
        Debug.Log("Quit");
    }
    
    public void Gameplay()
    {
        BStart.SetActive(false);
        QuitB.SetActive(false);
        ControlB.SetActive(false);
        GText.SetActive(true);
        BBack.SetActive(true);
    }
    public void Back()
    {
        BStart.SetActive(true);
        QuitB.SetActive(true);
        ControlB.SetActive(true);
        GText.SetActive(false);
        BBack.SetActive(false);
    }
}
