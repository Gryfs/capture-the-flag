﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental;
using UnityEngine;
using UnityEngine.UIElements;

public class ItemManager : MonoBehaviour
{
    public static ItemManager Instance;
    public List<GameObject> objectList = new List<GameObject>();
    public List<GameObject> spawner =new List<GameObject>();
    
    public List<GameObject> objselected = new List<GameObject>();

    public float timeur, timeurLimite;
    private void Awake()
    {
        Instance = this;
        InitSpawner();
    }

    public void Update()
    {
        timeur += Time.deltaTime;
        if (timeur > timeurLimite)
        {
            InitSpawner();
            timeur = 0;
        }
    }

    public void InitSpawner()
    {
        if (objselected[0] != null)
        {
            objectList.Add(objselected[0]);
            objselected[0] = null;
            
        }
        if (objselected[1] != null)
        {
            objectList.Add(objselected[1]);
            objselected[1] = null;
            
        }
        
        objselected[0] = objectList[Random.Range(0, objectList.Count)];
        objectList.Remove(objselected[0]);
        Instantiate(objselected[0], spawner[0].transform.position, Quaternion.identity);
        
        objselected[1] = objectList[Random.Range(0, objectList.Count)];
        objectList.Remove(objselected[1]);
        Instantiate(objselected[1], spawner[1].transform.position, Quaternion.identity);
    }
}
