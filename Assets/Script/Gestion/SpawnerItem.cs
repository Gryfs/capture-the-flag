﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerItem : MonoBehaviour
{
    public int rnd;

    public GameObject[] allItem;

    public float timer = 10;

    public GameObject objInstantiate;

    public GameObject objHere;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (timer >=0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            rnd = Random.Range(0,allItem.Length);
            Destroy(objHere);
            objInstantiate = allItem[rnd];
            objHere = Instantiate(objInstantiate, transform.position, Quaternion.identity);
            timer = 10;
        }
    }
}
