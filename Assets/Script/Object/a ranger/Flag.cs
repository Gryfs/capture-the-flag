﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour , IItem
{
    
     private PlayerInventory _inventory;

    public GameObject player;

    public bool isFlag;

    public bool setFlag;

    public GameObject otherFlag;
    // Start is called before the first frame update
    void Start()
    {
        _inventory = player.GetComponent<PlayerInventory>(); // find the inventory of the player
    }

    // Update is called once per frame

    void Update()
    {
       setFlag = player.GetComponent<ZoneVerif>().isOurZone; // find if the player is on his zone
        
       if (Input.GetButtonDown("Jump") && setFlag)
       {
           if (_inventory.inventory[0] == gameObject || _inventory.inventory[1] == gameObject) // use the function "Use" if the player is on his zone with a flag in the inventory
           {
               UseFunction();
           }
           
       }
        
       if (player.activeSelf == false && isFlag) // play the function "OnDeath" when the player die
       {
           OnDeath();
       }
    }

 /*   private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            if (setFlag == false)
            {


                if (other.gameObject.GetComponent<PlayerInventory>().inventory[0] == null &&
                    other.gameObject.GetComponent<PlayerInventory>().inventory[1] != otherFlag) // when the player collide with the flag and has no item in slot1 and no flag in slot2 he take it as first item
                {
                    Debug.Log("dfiv");
                    _inventory.inventory[0] = gameObject;
                    gameObject.GetComponent<MeshRenderer>().enabled = false;
                    gameObject.GetComponent<Collider>().enabled = false;
                    isFlag = true;
                }
                else if (other.gameObject.GetComponent<PlayerInventory>().inventory[1] == null &&
                         other.gameObject.GetComponent<PlayerInventory>().inventory[0] != otherFlag) // when the player collide with the flag and has no item in slot2 and no flag in slot1 he take it as second item
                {
                    _inventory.inventory[1] = gameObject;
                    gameObject.GetComponent<MeshRenderer>().enabled = false;
                    gameObject.GetComponent<Collider>().enabled = false;
                    isFlag = true;
                }
            }
        }
    }*/

    public void Use(GameObject playerUse, int inventoryIndex)
    {
        UseFunction();
    }

    void UseFunction() // set the flag and remove it from inventory
    {
        Vector3 position = player.transform.position;
        gameObject.transform.position = position + (2 * player.transform.forward);
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Collider>().enabled = true;
        gameObject.transform.parent = null;
            
        if (_inventory.inventory[0] == gameObject)
        {
            _inventory.inventory[0] = null;
        }

        if (_inventory.inventory[1] == gameObject)
        {
            _inventory.inventory[1] = null;
        }
        isFlag = false;
    }

    void OnDeath() // set the flag at the player position and remove it from inventory
    {
        Vector3 position = player.transform.position;
        gameObject.transform.position = position;
        
        if (_inventory.inventory[0] == gameObject)
        {
            _inventory.inventory[0] = null;
        }

        if (_inventory.inventory[1] == gameObject)
        {
            _inventory.inventory[1] = null;
        }
        
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Collider>().enabled = true;


        isFlag = false;
    }

}
