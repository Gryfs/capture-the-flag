﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Flag2 : MonoBehaviour
{
    private PlayerInventory _inventory;

    public GameObject ennemyPlayer;

    public bool isFlag;

    public GameObject otherFlag;
    
    public bool winCondition;
    // Start is called before the first frame update
    void Start()
    {
        _inventory = ennemyPlayer.GetComponent<PlayerInventory>(); // find the inventory of the player
    }

    // Update is called once per frame
    private void Update()
    {
        winCondition = ennemyPlayer.GetComponent<ZoneVerif>().isOurZone; // find if the player is on his zone
        
        if (Input.GetButtonDown("Jump") && winCondition)
        {
            if (_inventory.inventory[0] == gameObject || _inventory.inventory[1] == gameObject)// use the "Use" function if there is a flag in inventory and the player is on his zone
            {
                UseFunction();
            }
        }
        
        if (ennemyPlayer.activeSelf == false && isFlag) // use the "OnDeath" function when the player die
        {
            OnDeath();
        }
        
    }

   /* private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == ennemyPlayer)
        {
          
            Debug.Log("tri");
            if (other.gameObject.GetComponent<PlayerInventory>().inventory[0] == null 
                && other.gameObject.GetComponent<PlayerInventory>().inventory[1] != otherFlag) // when the player collide with the flag and has no item in slot1 and no flag in slot2 he take it as first item
            {
               Debug.Log("dfiv");
               _inventory.inventory[1] = gameObject;
               gameObject.GetComponent<MeshRenderer>().enabled = false;
               gameObject.GetComponent<Collider>().enabled = false;
               isFlag = true;
            } 
            if (other.gameObject.GetComponent<Inventory>().item1 == null
                && other.gameObject.GetComponent<Inventory>().item1 != otherFlag)// when the player collide with the flag and has no item in slot2 and no flag in slot1 he take it as second item
            {
               _inventory.item2 = gameObject;
               gameObject.GetComponent<MeshRenderer>().enabled = false;
               gameObject.GetComponent<Collider>().enabled = false;
               isFlag = true;
            }
        }
    }*/

   public void Use(GameObject playerUse, int inventoryIndex)
   {
       UseFunction();
   }
   
    void UseFunction() // set the flag and remove it from inventory and win the game
    {
        Vector3 position = ennemyPlayer.transform.position;
        gameObject.transform.position = position + (2* ennemyPlayer.transform.forward);
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Collider>().enabled = true;
        gameObject.transform.parent = null;
   
     if (_inventory.inventory[0] == gameObject)
     {
         _inventory.inventory[0] = null;
     }

     if (_inventory.inventory[1] == gameObject)
     {
         _inventory.inventory[1] = null;
     }

     SceneManager.LoadScene("MainMenu");
        isFlag = false;
        
    }

    void OnDeath() // set the flag at the player position and remove it from inventory
    {
        Vector3 position = ennemyPlayer.transform.position;
        gameObject.transform.position = position;
        if (_inventory.inventory[0] == gameObject)
        {
            _inventory.inventory[0] = null;
        }

        if (_inventory.inventory[1] == gameObject)
        {
            _inventory.inventory[1] = null;
        }
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Collider>().enabled = true;

        isFlag = false;
    }
    
}
