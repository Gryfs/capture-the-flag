﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeSC : MonoBehaviour, IItem
{
    //UI
    public string name;

    public int numberUsage;
    public bool canShoot;
    public float timeBeforeShoot;

    public GameObject knife;
    
    
    public void Use(GameObject playerUse, int inventoryIndex)
    {
        if (canShoot)
        {
            GameObject knifeInst = Instantiate(knife, playerUse.GetComponent<PlayerInventory>().firePoint.transform.position, playerUse.transform.localRotation );
            knifeInst.GetComponent<KnifeProjectile>().playerCast = playerUse;
            canShoot = false;
            numberUsage--;
            if (numberUsage <= 0)
            {
                playerUse.GetComponent<PlayerInventory>().inventory[inventoryIndex] = null;
                Destroy(gameObject);
            }
        }
    }

    public void Update()
    {
        if (!canShoot)
        {
            timeBeforeShoot += Time.deltaTime;
            if (timeBeforeShoot >= 3)
            {
                timeBeforeShoot = 0;
                canShoot = true;
            }
        }
    }
}

