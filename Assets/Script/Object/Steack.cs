﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steack : MonoBehaviour
{
    public int valueHealth;
    private void OnTriggerEnter(Collider other)
    {
        //if touch player stack are destroy and give 20PV of player who use
        if (other.gameObject.CompareTag("Player1")  || other.gameObject.CompareTag("Player2"))
        {
            other.GetComponent<PlayerMouvement>().health += valueHealth;
            Destroy(gameObject);
        }
 
    }
}

