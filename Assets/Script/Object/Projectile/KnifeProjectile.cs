﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeProjectile : MonoBehaviour
{
    public float speed;
    public GameObject playerCast;
    public int damageKnife;

    // Update is called once per frame
    void Start()
    {
        Destroy(gameObject, 0.2f);
    }
    void Update()
    {
        GetComponent<Rigidbody>().velocity = transform.TransformDirection( new Vector3( 0, 0, speed) );
    }

    private void OnTriggerEnter(Collider other)
    {
        //Todo ajouter la condition qui regarde si le player a un plastron ! 
        if (other.CompareTag("Player1") && other.gameObject.name != playerCast.name|| other.CompareTag("Player2")&& other.gameObject.name != playerCast.name)
        {
            if (other.gameObject.GetComponent<PlayerInventory>().chestPlate)
            {
                other.gameObject.GetComponent<PlayerInventory>().chestPlate = false;
                return;
            }
            Debug.Log(other.name);
            other.gameObject.transform.GetComponent<PlayerMouvement>().TakeDamage(damageKnife);
        }
        Destroy(gameObject);
    }
}