﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineGround : MonoBehaviour
{
    public float timerMine;

    public void Update()
    {
        timerMine += Time.deltaTime;

        if (timerMine > 3)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<SphereCollider>().enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player1" || other.tag == "Player2")
        {
            Debug.Log(other.name);
            other.gameObject.transform.GetComponent<PlayerMouvement>().TakeDamage(999);
            Destroy(gameObject);
        }
    }
}