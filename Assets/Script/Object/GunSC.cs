﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSC : MonoBehaviour, IItem
{
    //UI
    public string name;
    public int damageBullet;

    public int numberBullet;
    public void Use(GameObject playerUse, int inventoryIndex)
    {
        
        //PhysicsRaycast
        RaycastHit hit;
        if (Physics.Raycast(playerUse.GetComponent<PlayerInventory>().firePoint.transform.position, transform.TransformDirection(Vector3.forward), out hit,
            Mathf.Infinity))
        {
            numberBullet--;
            Debug.Log(hit.collider.name);
            //Todo ajouter la condition qui regarde si le player a un plastron ! 
            if (hit.collider.CompareTag("Player1")&& hit.collider.name != playerUse.name || hit.collider.CompareTag("Player2")&& hit.collider.name != playerUse.name )
            {
                if (hit.collider.transform.parent.gameObject.GetComponent<PlayerInventory>().chestPlate)
                {
                    hit.collider.transform.parent.gameObject.GetComponent<PlayerInventory>().chestPlate = false;
                    return;
                }
                hit.collider.transform.parent.gameObject.GetComponent<PlayerMouvement>().TakeDamage(damageBullet);
            }
        }
        else
        {
            Debug.Log("Do Nothing");
        }
        
        if (numberBullet <= 0)
        {
            playerUse.GetComponent<PlayerInventory>().inventory[inventoryIndex] = null;
            Destroy(gameObject);
        }
    }
}
