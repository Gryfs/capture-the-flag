﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootSC : MonoBehaviour, IItem
{
    //UI
    public string name;
    public GameObject playerUse;
    public int inventoryIndex;

    public float timeBeforeDestroy;
    public float timeBeforeDestroyLimit;

    public void Use(GameObject playerUse, int inventoryIndex)
    {
        
    }

    public void Start()
    {
        playerUse.GetComponent<PlayerMouvement>().speed += 3;
    }

    public void Update()
    {
        timeBeforeDestroy += Time.deltaTime;
        if (timeBeforeDestroy > timeBeforeDestroyLimit)
        {
            playerUse.GetComponent<PlayerInventory>().inventory[inventoryIndex] = null;
            playerUse.GetComponent<PlayerMouvement>().speed -= 3;
            Destroy(gameObject);
        }
    }
}
