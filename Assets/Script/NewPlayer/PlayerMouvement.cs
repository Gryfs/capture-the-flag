﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouvement : MonoBehaviour
{
    public float health = 100;
    // Start is called before the first frame update
    public CharacterController controller;
    
    private Quaternion targetRotation = Quaternion.identity;
    public float speed = 6f;

    public float horizontal;
    public float vertical;

    public string controllerH, controllerV;
    
    //public float turnSmoothTime = 0.2f;
    public float turnSmoothVelocity = 10;

    public int detectP;

    void Start()
    {
        if (gameObject.name == "Player1")
        {
            detectP = 1;
        }
        else
        {
            detectP = 2;
        }
    }

    void Update()
    {
        if (health > 100)
        {
            health = 100;
        }

        if (health <= 0)
        {
            StartCoroutine("Respawn");
        }
    }

    IEnumerator Respawn()
    {
        gameObject.GetComponent<PlayerMouvement>().enabled = false;
        

        if (detectP == 1)
        {
            gameObject.transform.position = new Vector3(-2.2f, 24.7f, 48); 
        }
        else
        {
            gameObject.transform.position = new Vector3(-6.6f, -11.05f, 6); 
        }
        yield return new WaitForSeconds(5);
        gameObject.GetComponent<PlayerMouvement>().enabled = true;
        

    }

    private void LateUpdate()
    {
        horizontal = Input.GetAxisRaw(controllerH);
        vertical = Input.GetAxisRaw(controllerV);                        //basic Movement
        Vector3 direction = new Vector3(0f, 0f, vertical);
        
        transform.Rotate(0.0f, Input.GetAxis (controllerH) * turnSmoothVelocity, 0.0f);
        /*if (horizontal != 0)
        {
            targetRotation *= Quaternion.AngleAxis(5, new Vector3(0f, horizontal, 0f));
            
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, turnSmoothVelocity * Time.deltaTime);*/

        
        //Permet d'avancer et de reculer en fonction de la rotation du player
        if (direction.magnitude >= 0.1f)
        {
            controller.Move(transform.forward * vertical  * (speed * Time.deltaTime));
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }
}