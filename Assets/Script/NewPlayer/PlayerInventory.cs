﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public List<GameObject> inventory = new List<GameObject>();

    public GameObject firePoint;
    public int posInInventory;

    public bool fullItemDrop;
    public GameObject itemFullItem;

    public bool chestPlate;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            posInInventory++;
            if (posInInventory > inventory.Count)
            {
                posInInventory = 0;
            }
        }
        
        if (Input.GetKeyDown(KeyCode.E) && inventory[posInInventory] != null)
        {
           inventory[posInInventory].GetComponent<IItem>().Use(gameObject, posInInventory);
        }

        if (fullItemDrop)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Destroy(inventory[posInInventory]);
                GameObject objTemp = Instantiate(itemFullItem.GetComponent<ItemLoot>().itemLoot, transform);
                inventory[posInInventory] = objTemp;
                if (objTemp.name.Contains("Boot"))
                {
                    objTemp.GetComponent<BootSC>().playerUse = gameObject;
                    objTemp.GetComponent<BootSC>().inventoryIndex = 1;
                }
                Destroy(itemFullItem);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object"))
        {
            if (other.GetComponent<ItemLoot>().itemLoot.name.Contains("ChestPlate"))
            {
                chestPlate = true;
                Destroy(other.gameObject);
            }
            else if (inventory[0] == null)
            {
                GameObject objTemp = Instantiate(other.GetComponent<ItemLoot>().itemLoot, transform);
                inventory[0] = objTemp;
                if (objTemp.name.Contains("Boot"))
                {
                    objTemp.GetComponent<BootSC>().playerUse = gameObject;
                    objTemp.GetComponent<BootSC>().inventoryIndex = 0;
                }
                Destroy(other.gameObject);
            } else if (inventory[1] == null)
            {
                GameObject objTemp = Instantiate(other.GetComponent<ItemLoot>().itemLoot, transform);
                inventory[1] = objTemp;
                if (objTemp.name.Contains("Boot"))
                {
                    objTemp.GetComponent<BootSC>().playerUse = gameObject;
                    objTemp.GetComponent<BootSC>().inventoryIndex = 1;
                }
                Destroy(other.gameObject);
            } else if (inventory[0] != null && inventory[1] != null)
            {
                fullItemDrop = true;
                itemFullItem = other.gameObject;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (fullItemDrop)
        {
            fullItemDrop = false;
            itemFullItem = null;
        }
    }
}
