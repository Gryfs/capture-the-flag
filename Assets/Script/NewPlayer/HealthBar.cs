﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{ 
    private Image HealthImage1;
    private Image HealthImage2;
    public float CurrentHealth1;
    public float CurrentHealth2;
    private float MaxHealth = 100f;
    public GameObject Player1;
    public GameObject Player2;
    void Start()
    {
        HealthImage1 =  GameObject.Find("Health P1").GetComponent<Image>();
        HealthImage2 =  GameObject.Find("Health P2").GetComponent<Image>();;
        Player1 = GameObject.Find("Player1");
        Player2 = GameObject.Find("Player2");

    }

    
    void Update()
    {
        CurrentHealth1 = Player1.GetComponent<PlayerMouvement>().health;
        CurrentHealth2 = Player2.GetComponent<PlayerMouvement>().health;
        HealthImage1.fillAmount = CurrentHealth1 / MaxHealth;
        HealthImage2.fillAmount = CurrentHealth2 / MaxHealth;
        
    }

}
